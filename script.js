const url_params = new URLSearchParams(window.location.search);
const tries = parseInt(url_params.get("t") || "5");
const secret = url_params.has("w") ? window.atob(url_params.get("w")).toUpperCase() : "FLO";
const letters = secret.length;

const SPACE = '0';

var cur_letter = [0, 0];
var won = false;
function get(i, j) { return document.getElementById("l" + i + j); }
function get_cur() { return get(cur_letter[0], cur_letter[1]); }
function modify_cur(new_cur) {
	if (cur_letter[1] != letters)
		get_cur().classList.remove("active");
	cur_letter = new_cur;
	if (cur_letter[1] != letters)
		get_cur().classList.add("active");
}

function init_board() {
	for (var i = 0; i < tries; i++) {
		var word = board.appendChild(document.createElement("div"));
		word.className = "word";
		for (var j = 0; j < letters; j++) {
			var letter = word.appendChild(document.createElement("div"));
			letter.id = "l" + i + j;
			letter.className = "letter";
		}
	}
	l00.classList.add("active");
}

function init_keyboard() {
	var format = ["QWERTZUIOP", "ASDFGHJKL", "+YXCVBNM-", SPACE];
	for (var i = 0; i < format.length; i++) {
		var row = keyboard.appendChild(document.createElement("div"));
		row.setAttribute("class", "keyrow");
		for (var j = 0; j < format[i].length; j++) {
			var key = row.appendChild(document.createElement("div"));
			var val = format[i][j];
			key.innerText = val == '+' ? "OK" :
				val == '-' ? "DEL" : val == SPACE ? "SPACE" : format[i][j];
			key.id = "k" + val;
			key.className = "key";
			key.onclick = (function(x) {
				return function() { press_letter(x); } })(val);
		}
	}
}

function init() {
	init_board();
	init_keyboard();
}

function guess(word) {
	var remaining = secret;
	function set_state(i, val, remove = true) {
		get(cur_letter[0], i).classList.add(val);
		if (remove)
			remaining = remaining.replace(word[i], "");		
	}
	for (var i = 0; i < letters; i++) {
		if (word[i] == secret[i])
			set_state(i, "fully_correct");
		if (!secret.includes(word[i]))
			document.getElementById("k" + (word[i] == ' ' ? SPACE : word[i])).
				classList.add("wrong");
	}
	for (var i = 0; i < letters; i++)
		if (word[i] != secret[i]) {
			if (remaining.includes(word[i]))
				set_state(i, "partially_correct");
			else
				set_state(i, "wrong", false);
		}
	return word == secret;	
}

function alert_result(text) {
	keyboard.innerHTML = "<span style=\"font-size: 200%\">" + text + "</span>";
}

function press_letter(x) {
	if (won)
		return;
	if (x == '-') {
		if (cur_letter[1] != 0) {
			modify_cur([cur_letter[0], cur_letter[1]-1]);
			get_cur().innerText = "";
		}
	} else if (x == '+') {
		if (cur_letter[1] == letters) {
			var word = "";
			for (var i = 0; i < letters; i++) {
				var letter = document.getElementById("l" + cur_letter[0] + i).innerText;
				word += letter.length == 0 ? ' ' : letter;
			}
			if (guess(word)) {
				won = true;
				alert_result("richtig. du bist so kluk! und flo ist kuuuhl");
			} else if (cur_letter[0] == tries-1)
				alert_result("schade marmelade");
			else
				modify_cur([cur_letter[0]+1, 0]);
		}
	} else if (cur_letter[1] != letters) {
		get_cur().innerText = x == SPACE ? ' ' : x;
		modify_cur([cur_letter[0], cur_letter[1]+1]);
	}
}

function create() {
	var word = own_word.value;
	if (word.length == 0 || !/^[a-zA-Z ]+$/.test(word) ||
			word.trim() != word || word.search("  ") != -1) {
		alert("Das Wort darf nur aus Buchstaben und Leerzeichen bestehen. Leerzeichen am Anfang/Ende oder mehr als eins zwischen zwei Wörtern ist nicht erlaubt.");
		return;
	}
	var tries = own_tries.value;
	if (tries.length == 0 || isNaN(tries) || parseInt(tries) < 1) {
		alert("Zumindest 1 Versuch.");
		return;
	}
	console.log(tries);
	word = window.btoa(word);
	own_url.value = window.location.href.split('?')[0] + "?w=" + word + "&t=" + tries;
	own_url.select();
}
